FROM python:alpine

WORKDIR /usr/src/app

RUN pip install requests

COPY freya.py .

CMD ["python3", "freya.py"] 
