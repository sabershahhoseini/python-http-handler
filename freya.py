import socketserver
import requests
import subprocess
import time
import re

from http.server import BaseHTTPRequestHandler

bot_token = #bot_token
chat_id = #bot_token
# url that telegram bot uses to send message
url = f'https://api.telegram.org/bot{bot_token}/sendMessage?chat_id={chat_id}&text='


class MyHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        print(self.path)
        regx = re.search('(/freya/)(.*)', str(self.path))
        if regx.group(1) == '/freya/':
            print('Message sent!')
            requests.get(f'{url}{regx.group(2)}')

        self.send_response(200)

httpd = socketserver.TCPServer(("0.0.0.0", 12732), MyHandler)
print(f"Started serving ...")
httpd.serve_forever()
